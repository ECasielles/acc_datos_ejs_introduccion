package com.mercacortex.ejsintroduccion_enriquecasielles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Ejercicio1_Act extends AppCompatActivity {

    EditText medtIn;
    RadioButton mrbtEuro, mrbtDolar;
    float conversionEuroDolar = 1.11865f;
    float valor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1_);

        medtIn = (EditText) findViewById(R.id.edtIn);

        mrbtEuro = (RadioButton) findViewById(R.id.rbtEuro);
        mrbtDolar = (RadioButton) findViewById(R.id.rbtDolar);
    }

    // Comprueba que el valor del cuadro de texto sea correcto y lo sustituye por el valor convertido
    public void radioClick(View v) {
        if (((RadioButton) v).isChecked() && medtIn.getText().toString() != null) {
            switch (v.getId()) {
                case R.id.rbtDolar:
                    valor = conversionEuroDolar * Float.parseFloat(medtIn.getText().toString());
                    medtIn.setText(String.valueOf(valor));
                    break;
                case R.id.rbtEuro:
                    valor = (1f / conversionEuroDolar) * Float.parseFloat(medtIn.getText().toString());
                    medtIn.setText(String.valueOf(valor));
                    break;
            }
        }
    }
}
