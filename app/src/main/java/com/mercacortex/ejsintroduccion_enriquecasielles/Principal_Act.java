package com.mercacortex.ejsintroduccion_enriquecasielles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Principal_Act extends AppCompatActivity implements View.OnClickListener {

    private Button mbtEj1, mbtEj2, mbtEj3, mbtEj4, mbtEj5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        mbtEj1 = (Button) findViewById(R.id.btnEj_1);
        mbtEj2 = (Button) findViewById(R.id.btnEj_2);
        mbtEj3 = (Button) findViewById(R.id.btnEj_3);
        mbtEj4 = (Button) findViewById(R.id.btnEj_4);
        mbtEj5 = (Button) findViewById(R.id.btnEj_5);

        mbtEj1.setOnClickListener(this);
        mbtEj2.setOnClickListener(this);
        mbtEj3.setOnClickListener(this);
        mbtEj4.setOnClickListener(this);
        mbtEj5.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent intent;
        switch (v.getId())
        {
            case R.id.btnEj_1:
                intent = new Intent(Principal_Act.this, Ejercicio1_Act.class);
                startActivity(intent);
                break;
            case R.id.btnEj_2:
                intent = new Intent(Principal_Act.this, Ejercicio2_Act.class);
                startActivity(intent);
                break;
            case R.id.btnEj_3:
                intent = new Intent(Principal_Act.this, Ejercicio3_Act.class);
                startActivity(intent);
                break;
            case R.id.btnEj_4:
                intent = new Intent(Principal_Act.this, Ejercicio4_Act.class);
                startActivity(intent);
                break;
            case R.id.btnEj_5:
                intent = new Intent(Principal_Act.this, Ejercicio5_Act.class);
                startActivity(intent);
                break;
        }
    }
}
