package com.mercacortex.ejsintroduccion_enriquecasielles;

public class Contador {

    //Tiempo en minutos
    int tiempo, cafes;

    public Contador(int tiempo, int cafes) {
        this.tiempo = tiempo;
        this.cafes = cafes;
    }

    public String disminuirTiempo(){
        if (tiempo >0)
            tiempo--;
        return String.valueOf(tiempo);
    }
    public String aumentarTiempo(){
        return String.valueOf(tiempo++);
    }
    public String aumentarCafes(){
        return String.valueOf(cafes++);
    }

    public int getTiempo() {
        return tiempo;
    }
    public int getCafes() {
        return cafes;
    }

}
