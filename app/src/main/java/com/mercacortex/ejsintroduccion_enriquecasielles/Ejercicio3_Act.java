package com.mercacortex.ejsintroduccion_enriquecasielles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class Ejercicio3_Act extends AppCompatActivity{

    Button mbtnLink;
    String url;
    EditText medtLink;
    WebView mmiWV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3_);

        medtLink = (EditText) findViewById(R.id.edtLink);
        mbtnLink = (Button) findViewById(R.id.btnLink);

        mbtnLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = ((EditText) findViewById(R.id.edtLink)).getText().toString();

                if (Patterns.WEB_URL.matcher(url).matches()) {
                    mmiWV = (WebView) findViewById(R.id.miWV);
                    mmiWV.loadUrl(url);
                }
            }
        });
    }
}
