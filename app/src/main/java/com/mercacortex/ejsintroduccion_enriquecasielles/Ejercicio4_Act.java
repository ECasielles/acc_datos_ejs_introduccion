package com.mercacortex.ejsintroduccion_enriquecasielles;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Ejercicio4_Act extends AppCompatActivity implements View.OnClickListener{

    Button mbtUp, mbtDown, mbtStart;
    TextView mtvCafes, mtvTiempo;
    Contador mcont;
    MyCountDownTimer miCrono;

    public static final int MINUTOS_INI = 5;
    public static final int CAFES_INI = 0;
    public static final int INTERVALO = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4_);

        mtvCafes = (TextView) findViewById(R.id.txvCount);
        mtvTiempo = (TextView) findViewById(R.id.txvTime);

        mcont = new Contador(MINUTOS_INI, CAFES_INI);

        mbtUp = (Button) findViewById(R.id.btnUp);
        mbtDown = (Button) findViewById(R.id.btnDown);
        mbtStart = (Button) findViewById(R.id.btnStart);

        mbtUp.setOnClickListener(this);
        mbtDown.setOnClickListener(this);
        mbtStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnUp:
                //Aumentar tiempo
                mtvTiempo.setText(mcont.aumentarTiempo() + ":00");
                break;
            case R.id.btnDown:
                //Disminuir tiempo
                mtvTiempo.setText(mcont.disminuirTiempo() + ":00");
                break;
            case R.id.btnStart:
                //Instancio mi clase de contador descendente y oculto los botones
                miCrono = new MyCountDownTimer(mcont.getTiempo()*60*1000, INTERVALO);
                miCrono.start();
                mbtUp.setEnabled(false);
                mbtDown.setEnabled(false);
                mbtStart.setEnabled(false);
                break;
        }
    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int minutos, segundos;
            minutos = (int) ((millisUntilFinished / 1000 ) / 60);
            segundos = (int) ((millisUntilFinished / 1000 ) % 60);
            mtvTiempo.setText(minutos + ":" + segundos);
        }

        @Override
        public void onFinish() {
            mtvTiempo.setText("Pausa terminada!!");
            mtvCafes.setText(mcont.aumentarCafes());
            mbtUp.setEnabled(true);
            mbtDown.setEnabled(true);
            mbtStart.setEnabled(true);
        }
    }
}
