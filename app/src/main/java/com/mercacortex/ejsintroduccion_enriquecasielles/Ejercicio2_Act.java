package com.mercacortex.ejsintroduccion_enriquecasielles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

public class Ejercicio2_Act extends AppCompatActivity {

    Switch mswUni;
    EditText medtMedida;
    float valor = 0f;
    float conversionCmInch = 2.54f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2_);

        medtMedida = (EditText) findViewById(R.id.edtnMedida);
        mswUni = (Switch) findViewById(R.id.swUni);

        mswUni.setChecked(false);

        mswUni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mswUni.setText("in");
                    valor = (1f / conversionCmInch) * Float.parseFloat(medtMedida.getText().toString());
                    medtMedida.setText(String.valueOf(valor));
                } else {
                    mswUni.setText("cm");
                    valor = conversionCmInch * Float.parseFloat(medtMedida.getText().toString());
                    medtMedida.setText(String.valueOf(valor));
                }
            }
        });
    }
}
